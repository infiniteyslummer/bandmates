class AddIndexToListings < ActiveRecord::Migration
  def change
      add_index :listings, :city
      add_index :listings, :state
  end
end
