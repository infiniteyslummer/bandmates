class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
      t.string :description
      t.string :city
      t.string :state
      t.string :tagone
      t.string :tagtwo
      t.string :tagthree
      t.references :user, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
