# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151023235940) do

  create_table "listings", force: :cascade do |t|
    t.string   "description"
    t.string   "city"
    t.string   "state"
    t.string   "tagone"
    t.string   "tagtwo"
    t.string   "tagthree"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "listings", ["city"], name: "index_listings_on_city"
  add_index "listings", ["state"], name: "index_listings_on_state"
  add_index "listings", ["user_id"], name: "index_listings_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",                   default: false
    t.string   "activation_digest"
    t.boolean  "activated",               default: true
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.string   "city"
    t.string   "state"
    t.integer  "soundcloud_user_id"
    t.string   "soundcloud_access_token"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["soundcloud_access_token"], name: "index_users_on_soundcloud_access_token"
  add_index "users", ["soundcloud_user_id"], name: "index_users_on_soundcloud_user_id"

end
