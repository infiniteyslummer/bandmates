User.create!(name:  "Brandon",
             email: "brandon.bbrandon@gmail.com",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true,
             activated: true,
             activated_at: Time.zone.now,
             city: "Long Beach",
             state: "CA")
99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now,
               city: "Long Beach",
               state: "CA")
end

#99.times do |l|
#  Listing.create!(user_id: l+1,
#               description:  "Need BandMate",
#               city: "city#{l}",
#               state: "state#{l}",
#               tagone: "one",
#               tagtwo: "two",
#               tagthree: "three")
#end
#
#10.times do |l|
#  Listing.create!(user_id: 10,
#               description:  "Need BandMate",
#               city: "Long Beach",
#               state: "CA",
#               tagone: "one",
#               tagtwo: "two",
#               tagthree: "three")
#end
#
#10.times do |l|
#  Listing.create!(user_id: 50,
#               description:  "Need BandMate",
#               city: "Long Beach",
#               state: "CA",
#               tagone: "one",
#               tagtwo: "two",
#               tagthree: "three")
#end
#
#10.times do |l|
#  Listing.create!(user_id: 76,
#               description:  "Need BandMate",
#               city: "Long Beach",
#               state: "CA",
#               tagone: "one",
#               tagtwo: "two",
#               tagthree: "three")
#end

