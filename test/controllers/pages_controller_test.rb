require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "BandMates"
  end

  test "should get help" do
    get :help
    assert_response :success
    assert_select "title", "Help | BandMates"
  end

end
