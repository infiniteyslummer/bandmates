class ListingsController < ApplicationController
    before_action :logged_in_user, only: [:new, :create]
    def new
        @listing = Listing.new
    end
    
    def index
        @listings = Listing.all.where(city: current_user.city, state: current_user.state).order(created_at: :desc)
        @user = current_user
        @SC = @user.soundcloud_access_token
        client = SoundCloud.new(:access_token => @SC)
        @tracks = client.get('/me/tracks', :limit => 3)
        @soundcloudid = @user.soundcloud_user_id
    end
    
    def create
        @listing = current_user.listings.build(listing_params)
      if @listing.save  
        flash[:success] = "New listing created!"
        redirect_to current_user
      else
          render 'new'
      end
    end
    
    def edit
       @listing = current_user.listings.find(params[:id])
    end
    
    def update
        @listing = current_user.listings.find(params[:id])
        if @listing.update_attributes(listing_params)
            flash[:success] = "Listing updated"
            redirect_to current_user
        else 
            render 'edit'
        end
    end
    
    def destroy
     Listing.find(params[:id]).destroy
     flash[:success] = "Listing destroyed"
     redirect_to current_user
    end
    
        private
    

        def listing_params
            params.require(:listing).permit(:description, :city, :state, :tagone, :tagtwo, :tagthree)
        end
end
