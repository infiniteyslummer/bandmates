class SoundcloudController < ApplicationController
    def connect
        # create client object with app credentials
        client = Soundcloud.new(:client_id => ENV["SOUNDCLOUD_CLIENT_ID"],
                                :client_secret => ENV["SOUNDCLOUD_CLIENT_SECRET"],
                                :redirect_uri => "http://localhost:3000/soundcloud/oauth-callback",
                                :response_type => 'code')

        # redirect user to authorize URL
        redirect_to client.authorize_url(:grant_type => 'authorization_code', :scope => 'non-expiring', :display => 'popup') 
    end

    def connected
                # create client object with app credentials
        client = Soundcloud.new(:client_id => ENV["SOUNDCLOUD_CLIENT_ID"],
                            :client_secret => ENV["SOUNDCLOUD_CLIENT_SECRET"],
                            :redirect_uri => "http://localhost:3000/soundcloud/oauth-callback")
        # exchange authorization code for access token
        access_token = client.exchange_token(:code => params[:code])
        client = Soundcloud.new(:access_token => access_token["access_token"])
        # make an authenticated call
        soundcloud_user = client.get('/me')
        unless User.where(:soundcloud_user_id => soundcloud_user["id"]).present?
#          user.create_from_soundcloud(soundcloud_user, access_token)
            current_user.update_attribute(:soundcloud_user_id, soundcloud_user["id"])
            current_user.update_attribute(:soundcloud_access_token, access_token["access_token"])
        end
        redirect_to current_user
    end

  def destroy
  end
    
    private
    
        def user_params
            params.require(:user).permit(:soundcloud_user_id, :soundcloud_access_token)
        end
end
