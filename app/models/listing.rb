class Listing < ActiveRecord::Base
    belongs_to :user
    validates :user_id, presence: true
    validates :city, presence: true
    validates :state, presence: true
    validates :description, presence: true, length: { maximum: 25 }
end
